---12/09/2023 | 12:28AM---
-"User Account" API created
-database currently not created yet and will be studied to connect the API, Mobile App, and Website w/
-will create more API for other parts/features of the app and website
-userinfo.json file is the reference for the informations the user/admin can interact with,
  will be modified if necessary.
-Used uuid for creating unique identifiers for each created accounts.
-Progression Folder for visual demonstration purposes, in case the viewer wants to see how the 
  current API works when receiving different commands without testing it with API platforms.
  (though feel free to try and testing it)
-tested the server using postman.
-dependencies used: 
  -uuid
  -express
  -nodemon
----------------------------------------------------------------------------------------------

