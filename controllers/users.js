import { v4 as uuidv4 } from 'uuid';

let users = [];

export const GetUsers = (req, res) => {
  res.send(users);
}

export const CreateUser = (req, res) => {
  const user = req.body;
  users.push({...user, userid: uuidv4()});
  res.send(`User with username : ${user.username} has been created.`);
}

export const GetUser = (req, res) => {
  const userid = req.params.userid;
  const founduser = users.find((user) => user.userid == userid);
  res.send(founduser);
}

export const DeleteUser = (req, res) => {
  const userid = req.params.userid;
  
  users = users.filter((user) => user.userid != userid);
  res.send(`User with the userID: ${userid} has been deleted from the database`);
}

export const UpdateUser = (req, res) => {
  const userid = req.params.userid;
  const { username, firstname, lastname, useremail, userpassword, userage, useraddress} = req.body;
  const userTBU = users.find((user) => user.userid == userid);

  if(username) userTBU.username = username;
  if(firstname) userTBU.firstname = firstname;
  if(lastname) userTBU.lastname = lastname;
  if(useremail) userTBU.usermail = useremail;
  if(userpassword) userTBU.userpassword = userpassword;
  if(userage) userTBU.userage = userage;
  if(useraddress) userTBU.useraddress = useraddress;

  res.send(`User with ID ${userid} has been updated`);
}