import express from 'express';
import { GetUsers, CreateUser, GetUser, DeleteUser, UpdateUser } from '../controllers/users.js';
const router = express.Router();

// all routes in here are starting with /users

router.get('/', GetUsers);

router.post('/', CreateUser);

router.get('/:userid', GetUser);

router.delete('/:userid', DeleteUser);

router.patch('/:userid', UpdateUser);
 
export default router;